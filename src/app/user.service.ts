import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from './user';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private baseUrl = 'http://localhost:8086/api/users';
  private bearer = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzaWduZWQiOiJ0ZXN0LWF1dGgiLCJpYXQiOjE1ODMzMzYxMjgsImV4cCI6MTU4MzQyMjUyOH0.v1eTPsqQ7qLcWCetaumd-4YuaVJj8oHFWm_QrTQLqkE';

  constructor(private http: HttpClient) {
  }

  getUser(id: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`, {
      headers: new HttpHeaders({
        'Authorization': this.bearer
      })
    });
  }

  createUser(user: object): Observable<object> {
    return this.http.post(`${this.baseUrl}`, user, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.bearer
      })
    });
  }

  updateUser(id: string, value: any): Observable<object> {
    return this.http.put(`${this.baseUrl}/${id}`, value, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.bearer
      })
    });
  }

  deleteUser(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}`, {
      params: {
        ids: [id]
      },
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.bearer
      })
    });
  }

  getUsersList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`, {
      headers: new HttpHeaders({
        'Authorization': this.bearer
      })
    });
  }
}
