export class User {
  id: string;
  name: string;
  username: string;
  email: string;
  user_group: string;
  enabled: boolean;
  activated: boolean;
  created_at: Date;
  last_visit: Date;
}
