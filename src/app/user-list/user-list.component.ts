import {UserDetailsComponent} from '../user-details/user-details.component';
import {Observable} from 'rxjs';
import {UserService} from '../user.service';
import {User} from '../user';
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  users: [];

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.userService.getUsersList().subscribe(data => {
      this.users = data.data.rows;
    });
  }

  deleteUser(id: string) {
    this.userService.deleteUser(id).subscribe(
      data => {
        console.log(data);
        this.reloadData();
      },
      error => console.log(error)
    );
  }

  userDetails(id: string) {
    this.router.navigate(['details', id]);
  }
}
